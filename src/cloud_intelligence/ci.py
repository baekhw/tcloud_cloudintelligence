import socket
import logging
import time
import xmlrpclib
from SimpleXMLRPCServer import SimpleXMLRPCServer

import MySQLdb

import sys
import pwd
import getopt
import os
import re
#import xmlrpclib
from M2Crypto import X509

#CI_IP_ADDR = "10.1.3.2"
CI_IP_ADDR = "0.0.0.0"
TM_IP_ADDR = "10.1.3.3"
NM_IP_ADDR = "10.1.3.4"
DB_IP_ADDR = TM_IP_ADDR
DB_USER = "ci"
DB_PASSWD = "123456"
DB_NAME = "demo"
DB_PORT = 3306
#LOG_PATH = '/users/Weiyu/2.ciNode/log/stateMachineLog.txt'
LOG_PATH = '/tmp/stateMachineLog.txt'
CRED_SOTRAGE_PATH = '/users/elabman/'

#config the format of the log
logging.basicConfig(filename = LOG_PATH,level = logging.DEBUG, filemode = 'a', format = '%(asctime)s - %(levelname)s: %(message)s')

server = SimpleXMLRPCServer((CI_IP_ADDR, 8001))

state0 = "normal"
state1 = "alarm1"
state2 = "alarm2"
state = state0

#server in moniNode
tm_proxy = xmlrpclib.ServerProxy("http://" + TM_IP_ADDR + ":8000/")
nm_proxy = None

#conn to DB on moniNode
conn = None
cur = None

execfile( "test-common.py" )
		
def init_db() :
	conn=MySQLdb.connect(host=DB_IP_ADDR,user=DB_USER,passwd=DB_PASSWD,db=DB_NAME,port=DB_PORT)
	cur = conn.cursor()

def find_source(src_ip,src_port,dst_ip,dst_port):
	sys_argv = sys.argv
	active_host_list = []
	source_host = None
	suspecious_host_list = []
	
	#for filename in os.listdir(CRED_SOTRAGE_PATH):
	for filename in ["baekhw.cred"] :
		if not ".cred" in filename:
			continue
		cred_filename = filename
		print cred_filename
		temp_argv = [None,'-a',CRED_SOTRAGE_PATH+cred_filename]
		print temp_argv[2]
		sys.argv = temp_argv

		#if admincredentialfile:
		f = open( CRED_SOTRAGE_PATH+cred_filename )
		mycredential = f.read()
		f.close()
# 		else:
# 			Fatal("You need to supply an admin credential");
# 			pass
		params = {}
		params["credentials"] = (mycredential,)
		rval,response = do_method("cm", "ListActiveSlivers", params)
		if rval:
			Fatal("Could not get a list of resources")
			pass
		hostname = response["value"][0]["manifest"].split('login')[1].split('hostname=')[1].replace("'",'"').split('"')[1]
		#vmname =   response["value"][0]["manifest"].split('emulab:vnode')[1].split('name=')[1].replace("'",'"').split('"')[1]
		if not hostname in active_host_list:
			active_host_list.append(hostname)
		hostip = socket.gethostbyname(hostname)
		if(hostip==src_ip):
			source_host = src_ip
	if source_host!=None :
		suspecious_host_list.append(source_host)
	else :
		suspecious_host_list = active_host_list
	proc_list = []
	for host in suspecious_host_list :
		nm_proxy = xmlrpclib.ServerProxy("http://" + host + ":8001/")
		proc_list += nm_proxy.find_source(src_ip,src_port,dst_ip,dst_port)
	return proc_list		
		
		
class stateMachine():
	def stateTransition(self, msg):
		print ("got msg %s from moniNode" %msg)
		global state
		if(msg == state0 or msg == state1 or msg == state2):
			state = msg
			print("set state to %s" %state)
			if(state == state2) :
				src,dst = self.selectDataFromRemoteDB()
				print "src : " + str(src)+ "\n dst : " + str(dst)
 				print "params"
				print "src : " + str(src)+ "\n dst : " + str(dst)
 				try :
 						#proc_list = nm_proxy.find_source(src.split(":")[0],int(src.split(":")[1]),
 						#												dst.split(":")[0],int(dst.split(":")[1]))
 						find_source(src.split(":")[0],int(src.split(":")[1]),
 																		dst.split(":")[0],int(dst.split(":")[1]))
						print "proc_list"
						for proc in proc_list :
								print proc
				except :
						print "Value Error"
						return state
		else:
			logging.debug("invalide msg from moniNode: %s" %state)
		return state
	
	def getState(self):
		global state
		return state
	
	def selectDataFromRemoteDB(self):
		cur.execute("""SELECT * FROM addrTable ORDER BY time DESC, count DESC LIMIT 1""")
		row = cur.fetchall()
		return row[0][1],row[0][2]
# 		time = row[0][0]
# 		count = row[0][3]
# 		
# 		cur.execute("""SELECT * FROM addrTable WHERE time = %s AND count = %s""", (time, count))
# 		rows = cur.fetchall()
# 		print "+-------time-------+--srcIP:port-+-dstIP:port-+count+"
# 		for record in rows:
# 			print record
	
if __name__ == "__main__":
#	s = stateMachine()
#	s.selectDataFromRemoteDB()
	server.register_instance(stateMachine())
	server.serve_forever()
