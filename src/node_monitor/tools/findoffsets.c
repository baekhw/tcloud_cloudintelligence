/* The LibVMI Library is an introspection library that simplifies access to 
 * memory in a target virtual machine or in a file containing a dump of 
 * a system's physical memory.  LibVMI is based on the XenAccess Library.
 *
 * Copyright 2011 Sandia Corporation. Under the terms of Contract
 * DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government
 * retains certain rights in this software.
 *
 * Author: Bryan D. Payne (bdpayne@acm.org)
 *
 * This file is part of LibVMI.
 *
 * LibVMI is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * LibVMI is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with LibVMI.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/sched.h>
#include <linux/fdtable.h>
#include <linux/types.h>
#include <linux/sched.h>
#include <linux/rcupdate.h>
#include <linux/fdtable.h>
#include <linux/fs.h>
#include <linux/fs_struct.h>
#include <linux/dcache.h>
#include <linux/slab.h>
#include <linux/stat.h>
#include <linux/net.h>
#include <linux/socket.h>
#include <net/inet_sock.h>

#define MYMODNAME "FindOffsets "

static int my_init_module(void);
static void my_cleanup_module(void);

static int my_init_module(void) {
    struct task_struct *p = NULL;
    struct task_struct *next_p = NULL;
    unsigned long offset_p_comm;
    unsigned long offset_p_tasks;
    unsigned long offset_p_mm;
    unsigned long offset_p_pid;
    unsigned long offset_mm_pgd;
    unsigned long offset_mm_startcode;

    unsigned long offset_p_files;
    unsigned long offset_files_fdt;
    unsigned long offset_fdt_fd;
    unsigned long offset_fd_dentry;
    unsigned long offset_dentry_inode;
    unsigned long offset_inode_mode;
    unsigned char found_all_file_offsets = 0;

    unsigned long socket_size;
    unsigned long i_mode_mask;
    unsigned long i_mode_sock;
    int sock_type_raw;

    unsigned long offset_socket_type;
    unsigned long offset_socket_inetsock;
    unsigned long offset_inetsock_family;
    unsigned long offset_inetsock_rcvsaddr;
    unsigned long offset_inetsock_sport;
    unsigned long offset_inetsock_daddr;
    unsigned long offset_inetsock_dport;

    struct socket tmp_sock;
    struct inet_sock tmp_inetsock;
    struct socket *p_sock;
    struct inet_sock *p_inet_sock;

	struct files_struct *current_files;
	struct fdtable *files_table;
	int i=0;
	struct path files_path;
	char *cwd;
	char *buf;
    int first_pid;

    printk(KERN_ALERT "Module %s loaded.\n\n", MYMODNAME);
    p = current;
    if (p != NULL) {
        offset_p_comm = (unsigned long) (&(p->comm)) - (unsigned long) (p);
        offset_p_tasks =
            (unsigned long) (&(p->tasks)) - (unsigned long) (p);
        offset_p_mm = (unsigned long) (&(p->mm)) - (unsigned long) (p);
        offset_p_pid = (unsigned long) (&(p->pid)) - (unsigned long) (p);
        offset_mm_pgd =
            (unsigned long) (&(p->mm->pgd)) - (unsigned long) (p->mm);
        offset_mm_startcode =
            (unsigned long) (&(p->mm->start_code)) -
            (unsigned long) (p->mm);

        printk(KERN_ALERT "[domain name] {\n");
        printk(KERN_ALERT "    ostype = \"Linux\";\n");
        printk(KERN_ALERT "    sysmap = \"[insert path here]\";\n");
        printk(KERN_ALERT "    linux_name = 0x%x;\n",
               (unsigned int) offset_p_comm);
        printk(KERN_ALERT "    linux_tasks = 0x%x;\n",
               (unsigned int) offset_p_tasks);
        printk(KERN_ALERT "    linux_mm = 0x%x;\n",
               (unsigned int) offset_p_mm);
        printk(KERN_ALERT "    linux_pid = 0x%x;\n",
               (unsigned int) offset_p_pid);
        printk(KERN_ALERT "    linux_pgd = 0x%x;\n",
               (unsigned int) offset_mm_pgd);
        printk(KERN_ALERT "    linux_addr = 0x%x;\n",
               (unsigned int) offset_mm_startcode);
        printk(KERN_ALERT "}\n");

        offset_p_files = (unsigned long) (&(p->files)) - (unsigned long) (p);
        offset_files_fdt = (unsigned long) (&(p->files->fdt)) - (unsigned long)(p->files);
        offset_fdt_fd = (unsigned long) (&(p->files->fdt->fd)) - (unsigned long)(p->files->fdt);
        socket_size = (unsigned long) sizeof(struct socket);
        i_mode_mask = S_IFMT;
        i_mode_sock = S_IFSOCK;
        sock_type_raw = SOCK_RAW;

        printk(KERN_ALERT "proof check\n");
		printk(KERN_ALERT "name : %s\n",(char*)(p->comm));
		printk(KERN_ALERT "pid : %d\n",(int)(p->pid));
		printk(KERN_ALERT "file count : %d\n",(int)(p->files->count.counter));
		first_pid = p->pid;
	    while (p != NULL) {
	    	next_p = (struct task_struct *)((unsigned long)(p->tasks.next) - offset_p_tasks);
	    	p = next_p ;
	    	printk(KERN_ALERT "name : [%5d] %20s \n", p->pid, (char*)(p->comm));
			buf = (char *)kmalloc(GFP_KERNEL,100*sizeof(char));
			current_files = p->files;
			files_table = files_fdtable(current_files);
			i = 0;
			while(files_table->fd[i] != NULL) {
				if (found_all_file_offsets == 0) { //if not found all the offsets yet.
					offset_fd_dentry = (unsigned long) (&(files_table->fd[i]->f_dentry)) - (unsigned long)(files_table->fd[i]);
					offset_dentry_inode = (unsigned long) (&(files_table->fd[i]->f_dentry->d_inode)) - (unsigned long)(files_table->fd[i]->f_dentry);
					offset_inode_mode = (unsigned long) (&(files_table->fd[i]->f_dentry->d_inode->i_mode)) - (unsigned long)(files_table->fd[i]->f_dentry->d_inode);
					found_all_file_offsets = 1; //mark that we found all the offsets.
				}
				printk(KERN_ALERT "\n  - current file address : 0x%lx", (unsigned long)(files_table->fd[i]));
				//printk(KERN_ALERT "    current pointer address : 0x%lx", (unsigned long)&(files_table->fd[i]));
				files_path = files_table->fd[i]->f_path;
				cwd = d_path(&files_path,buf,100*sizeof(char));
				printk(KERN_ALERT "    Open file with fd %d  %s", i,cwd);
				//printk(KERN_ALERT "i_mode of fd : 0%o", files_table->fd[i]->f_dentry->d_inode->i_mode);
				//printk(KERN_ALERT "i_mode_sock : 0%o",i_mode_sock);
				//printk(KERN_ALERT "maksed i_mode : 0%o",files_table->fd[i]->f_dentry->d_inode->i_mode & i_mode_mask);
				printk(KERN_ALERT "        current inode is at 0x%lx", (unsigned long)files_table->fd[i]->f_dentry->d_inode);
				if ((files_table->fd[i]->f_dentry->d_inode->i_mode & i_mode_mask) == i_mode_sock) { //socket inode
					printk(KERN_ALERT "        Socket!! Yeah!\n");
					p_sock = (struct socket *)(((unsigned long)files_table->fd[i]->f_dentry->d_inode) - socket_size);
					printk(KERN_ALERT "        Struct Socket is at 0x%lx!\n",(unsigned long)p_sock);
					printk(KERN_ALERT "        Socket Type   is at 0x%lx!\n",(unsigned long)&(p_sock->type));
					printk(KERN_ALERT "        Socket Type size is %d!\n",(int)sizeof(p_sock->type));
					if (p_sock->type == sock_type_raw) //p_sock->sk is just struct sock
						printk(KERN_ALERT "        Raw Sock Type\n");
					else { //p_sock->sk is struct inet_sock
						printk(KERN_ALERT "        sock type : 0x%x", p_sock->type);
						p_inet_sock = (struct inet_sock *)(p_sock->sk);
						printk(KERN_ALERT "      Socket family is at 0x%lx",(unsigned long)&(p_inet_sock->sk.__sk_common.skc_family));
						printk(KERN_ALERT "      family size is %d",sizeof(p_inet_sock->sk.__sk_common.skc_family));
						if (p_inet_sock->sk.__sk_common.skc_family == AF_INET) {
							printk(KERN_ALERT "        IPv4!!");
							printk(KERN_ALERT "        src addr : 0x%x", p_inet_sock->inet_rcv_saddr);
							printk(KERN_ALERT "        src port : 0x%x", p_inet_sock->inet_sport);
							printk(KERN_ALERT "        dst addr : 0x%x", p_inet_sock->inet_daddr);
							printk(KERN_ALERT "        dst port : 0x%x", p_inet_sock->inet_dport);
						}
						else
							printk(KERN_ALERT "        addr fam : %d", p_inet_sock->sk.__sk_common.skc_family);
					}
				}
				i++;
			}
			//printk(KERN_ALERT "null pointer data : 0x%lx", (unsigned long)(files_table->fd[i]));
			//printk(KERN_ALERT "null pointer address : 0x%lx", (unsigned long)&(files_table->fd[i]));
	    	if (next_p->pid == first_pid)
	    		break;
	    }

	    offset_socket_type = (unsigned long)&(tmp_sock.type) - (unsigned long)&tmp_sock;
		offset_socket_inetsock = (unsigned long)&(tmp_sock.sk) - (unsigned long)&tmp_sock;
		offset_inetsock_family = (unsigned long)&(tmp_inetsock.sk.__sk_common.skc_family) - (unsigned long)&tmp_inetsock;
		offset_inetsock_rcvsaddr = (unsigned long)&(tmp_inetsock.inet_rcv_saddr) - (unsigned long)&tmp_inetsock;
		offset_inetsock_sport = (unsigned long)&(tmp_inetsock.inet_sport) - (unsigned long)&tmp_inetsock;
		offset_inetsock_daddr = (unsigned long)&(tmp_inetsock.inet_daddr) - (unsigned long)&tmp_inetsock;
		offset_inetsock_dport = (unsigned long)&(tmp_inetsock.inet_dport) - (unsigned long)&tmp_inetsock;

        printk(KERN_ALERT "[Custom Offsets and values] {");
        printk(KERN_ALERT "    linux_files = 0x%x;\n",(unsigned int)offset_p_files);
        printk(KERN_ALERT "    linux_files_fdt = 0x%x;\n",(unsigned int)offset_files_fdt);
        printk(KERN_ALERT "    linux_fdt_fd = 0x%x;\n",(unsigned int)offset_fdt_fd);
        printk(KERN_ALERT "    linux_fd_dentry = 0x%x;\n",(unsigned int)offset_fd_dentry);
        printk(KERN_ALERT "    linux_dentry_inode = 0x%x;\n",(unsigned int)offset_dentry_inode);
        printk(KERN_ALERT "    linux_inode_mode = 0x%x;\n",(unsigned int)offset_inode_mode);
        printk(KERN_ALERT "    linux_socket_size = 0x%x;\n",(unsigned int)socket_size);
        printk(KERN_ALERT "    offset_socket_type = 0x%x;\n",(unsigned int)offset_socket_type);
        printk(KERN_ALERT "    offset_socket_inetsock = 0x%x;\n",(unsigned int)offset_socket_inetsock);
        printk(KERN_ALERT "    offset_inetsock_family = 0x%x;\n",(unsigned int)offset_inetsock_family);
        printk(KERN_ALERT "    offset_inetsock_rcvsaddr = 0x%x;\n",(unsigned int)offset_inetsock_rcvsaddr);
        printk(KERN_ALERT "    offset_inetsock_sport = 0x%x;\n",(unsigned int)offset_inetsock_sport);
        printk(KERN_ALERT "    offset_inetsock_daddr = 0x%x;\n",(unsigned int)offset_inetsock_daddr);
        printk(KERN_ALERT "    offset_inetsock_dport = 0x%x;\n",(unsigned int)offset_inetsock_dport);
        printk(KERN_ALERT "    i_mode_MASK = 0x%x;\n",(unsigned int)i_mode_mask);
		printk(KERN_ALERT "    i_mode_SOCK = 0x%x;\n",(unsigned int)i_mode_sock);
		printk(KERN_ALERT "    socket_size = 0x%x;\n",(unsigned int)socket_size);
        printk(KERN_ALERT "    sock_type_RAW = %d;\n",(int)sock_type_raw);
        printk(KERN_ALERT "    inetsock_family_AFINET = %d;\n",(int)AF_INET);
        printk(KERN_ALERT "}\n");
    }
    else {
        printk(KERN_ALERT
               "%s: found no process to populate task_struct.\n",
               MYMODNAME);//asdf
    }
    printk(KERN_ALERT "hello world!2");
    return 0;
}

static void
my_cleanup_module(
    void)
{
    printk(KERN_ALERT "Module %s unloaded.\n", MYMODNAME);
}

module_init(my_init_module);
module_exit(my_cleanup_module);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Mod by Hyun-wook Baek from the offset finder of Nilushan Silva");
MODULE_DESCRIPTION("offset Finder");
