import subprocess

def get_vm_list() :
    proc = subprocess.Popen(['xm','list'],stdout=subprocess.PIPE)
    vm_list = []
    proc.stdout.readline() #skip the first line
    proc.stdout.readline() #skip the second line (Dom0)
    while True:
        line = proc.stdout.readline()
        if line == '' :
            break
        vm_list.append(line.split()[0])
    return vm_list
  
def get_datalink_addr_list(vm_name):
    proc = subprocess.Popen(['xm','network-list',vm_name],stdout=subprocess.PIPE)
    network_list = []
    proc.stdout.readline() #skip the first line
    while True:
        line = proc.stdout.readline()
        if line == '' :
            break
        network_list.append(line.split()[2])
    return network_list
  
def get_ip(datalink_addr): # TO-DO : to improve performance, we should avoid looking up the ARP tables repeatedly.
    proc = subprocess.Popen(['arp','-an'],stdout=subprocess.PIPE)
    while True:
        line = proc.stdout.readline()
        if line == '' :
            break
        line_data = line.split()
        ip = line_data[1][1:-1]
        mac = line_data[3]
        if (mac == datalink_addr) :
            return ip
    return None

if __name__ == "__main__":
    print get_vm_list()