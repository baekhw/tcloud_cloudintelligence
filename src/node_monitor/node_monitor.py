#!/usr/bin/env python

import pyvmi
import sys
import xmlrpclib
import socket
import struct
import subprocess
from SimpleXMLRPCServer import SimpleXMLRPCServer

# libvmi does not support extra offset values.
# need to make a customized conf file for offering extra offsets.
OFFSET_LINUX_PROCESS_FILES = 0x550
OFFSET_LINUX_FILES_FDT = 0x8
OFFSET_LINUX_FDT_FD = 0x8
OFFSET_LINUX_FD_DENTRY = 0x18
OFFSET_LINUX_DENTRY_INODE = 0x30
OFFSET_LINUX_INODE_MODE = 0x0
OFFSET_LINUX_SOCKET_TYPE = 0x4;
OFFSET_LINUX_SOCKET_INETSOCK = 0x20;
OFFSET_LINUX_INETSOCK_FAMILY = 0xc;
OFFSET_LINUX_INETSOCK_RCVSADDR = 0x4;
OFFSET_LINUX_INETSOCK_SPORT = 0x284;
OFFSET_LINUX_INETSOCK_DADDR = 0x0;
OFFSET_LINUX_INETSOCK_DPORT = 0x278;
I_MODE_MASK = 0xf000;
I_MODE_SOCK = 0xc000;
LINUX_SOCKET_SIZE = 0x30;
SOCK_TYPE_RAW = 3;
INETSOCK_FAMILY_AFINET = 2;
ADDRESS_SIZE = 8

server = None

class Socket_process_info (object):
    vm,pid,procname,saddr,sport,daddr,dport = None,None,None,None,None,None,None
    def __init__(self,vm,pid,procname,saddr,sport,daddr,dport):
        self.vm = vm
        self.pid = pid
        self.procname = procname
        self.saddr = saddr
        self.sport = sport
        self.daddr = daddr
        self.dport = dport
    def __str__(self):
        rtn = "VM name : " + self.vm
        rtn += "\nPID : " + str(self.pid)
        rtn += "\nProcess name : " + self.procname
        rtn += "\nSource Addr : " + self.saddr + ":" + str(self.sport)
        rtn += "\nDestination Addr : " + self.daddr + ":" + str(self.dport)
        return rtn
        
    
class Inetsock_navigator (object):
    vmi = None
    current_inetsock = 0
    family_offset = 0
    saddr_offset = 0
    daddr_offset = 0
    sport_offset = 0
    dport_offset = 0
    def __init__(self,vmi,current_inetsock):
        global OFFSET_LINUX_INETSOCK_FAMILY
        global OFFSET_LINUX_INETSOCK_RCVSADDR
        global OFFSET_LINUX_INETSOCK_SPORT
        global OFFSET_LINUX_INETSOCK_DADDR
        global OFFSET_LINUX_INETSOCK_DPORT
        self.vmi = vmi
        self.current_inetsock = current_inetsock
        self.family_offset = OFFSET_LINUX_INETSOCK_FAMILY
        self.saddr_offset = OFFSET_LINUX_INETSOCK_RCVSADDR
        self.daddr_offset = OFFSET_LINUX_INETSOCK_DADDR
        self.sport_offset = OFFSET_LINUX_INETSOCK_SPORT
        self.dport_offset = OFFSET_LINUX_INETSOCK_DPORT
        
    def get_family(self):
        #print "      family is at 0x%x" % (self.current_inetsock + self.family_offset)
        return self.vmi.read_16_va(self.current_inetsock + self.family_offset,0)

    def get_saddr(self):
        return self.vmi.read_32_va(self.current_inetsock + self.saddr_offset,0)

    def get_daddr(self):
        return self.vmi.read_32_va(self.current_inetsock + self.daddr_offset,0)

    def get_sport(self):
        return self.vmi.read_16_va(self.current_inetsock + self.sport_offset,0)

    def get_dport(self):
        return self.vmi.read_16_va(self.current_inetsock + self.dport_offset,0)

class Socket_navigator (object) :
    vmi = None
    current_socket = 0
    type_offset = 0
    inetsocket_offset = 0
    def __init__(self,vmi,current_socket):
        global OFFSET_LINUX_SOCKET_TYPE
        global OFFSET_LINUX_SOCKET_INETSOCK
        self.vmi = vmi
        self.current_socket = current_socket
        self.type_offset = OFFSET_LINUX_SOCKET_TYPE
        self.inetsocket_offset = OFFSET_LINUX_SOCKET_INETSOCK
    
    def get_type(self):
        #print '      Struct Socket is at 0x%x' % (self.current_socket)
        #print '      Socket Type   is at 0x%x' % (self.current_socket + self.type_offset)
        return self.vmi.read_16_va(self.current_socket + self.type_offset,0)
    
    def get_inetsock(self):
        return self.vmi.read_addr_va(self.current_socket + self.inetsocket_offset,0)
      

class Inode_navigator (object) :
    vmi = None
    current_inode = 0
    i_mode_offset = 0
    def __init__(self,vmi,current_inode):
        global OFFSET_LINUX_INODE_MODE
        global LINUX_SOCKET_SIZE
        self.vmi = vmi
        self.current_inode = current_inode
        self.i_mode_offset = OFFSET_LINUX_INODE_MODE
        self.socket_size = LINUX_SOCKET_SIZE
    
    def get_i_mode(self):
        try :
            temp = self.vmi.read_16_va(self.current_inode + self.i_mode_offset,0)
            return temp
        except ValueError :
            return 0
    
    def get_socket(self):
        # Ref. Linux socket container structure from sock.h
        #   struct socket_alloc {
        #       struct socket socket;
        #       struct inode vfs_inode;
        #   };
        try :
            #print '      current inode is at 0x%x' % (self.current_inode)
            #print '      current socket is at 0x%x' % (self.current_inode - self.socket_size)
            return self.current_inode - self.socket_size
        except ValueError :
            return 0

class Dentry_navigator (object):
    vmi = None
    current_dentry = 0 # Virtual Address of struct dentry object
    d_inode_offset = 0
    def __init__(self,vmi,current_dentry):
        global OFFSET_LINUX_DENTRY_INODE
        self.vmi = vmi
        self.current_dentry = current_dentry
        self.d_inode_offset = OFFSET_LINUX_DENTRY_INODE
        
    def get_d_inode(self):
        try :
            return self.vmi.read_addr_va(self.current_dentry + self.d_inode_offset,0)
        except ValueError :
            return 0 
    
class File_navigator (object):
    vmi = None
    current_file = 0 # Virtual Address of struct file object
    f_dentry_offset = 0
    def __init__(self,vmi,current_file):
        global OFFSET_LINUX_FD_DENTRY
        self.vmi = vmi
        self.current_file = current_file
        self.f_dentry_offset = OFFSET_LINUX_FD_DENTRY
    
    def get_f_dentry(self):
        return self.vmi.read_addr_va(self.current_file + self.f_dentry_offset,0)

class Fdtable_navigator (object):
    vmi = None
    current_fdtable = 0 # Virtual Address of fdtable object
    fd_offset = 0
    address_size = 0
    
    def __init__(self,vmi,current_fdtable):
        global OFFSET_LINUX_FDT_FD
        global ADDRESS_SIZE
        self.vmi = vmi
        self.current_fdtable = current_fdtable
        # self.fd_offset = vmi.get_offset("linux_fd") # libvmi does not support extra offset values.
        self.fd_offset = OFFSET_LINUX_FDT_FD
        self.address_size = ADDRESS_SIZE
    
    def get_files(self):
        count = 0
        fd_array = self.vmi.read_addr_va(self.current_fdtable + self.fd_offset,0)
        current_file = self.vmi.read_addr_va(fd_array,0)
        while current_file != 0 :
            count += 1
            yield current_file
            current_file = self.vmi.read_addr_va(fd_array + self.address_size * count,0)
         
    def get_file_count(self):
        count = 0
        fd_array = self.vmi.read_addr_va(self.current_fdtable + self.fd_offset,0)
        current_file = self.vmi.read_addr_va(fd_array,0)
        while current_file != 0 :
            count += 1
            #print "current_file address : 0x%x" % (current_file)
            current_file = self.vmi.read_addr_va(fd_array + self.address_size * count,0)
        return count
    
class Files_struct_navigator (object):
    vmi = None
    current_files_struct = 0 # Virtual Address of files_struct object
    fdt_offset = 0
        
    def __init__(self,vmi,current_files_struct):
        global OFFSET_LINUX_FILES_FDT
        self.vmi = vmi
        self.current_files_struct = current_files_struct
        # self.fdt_offset = vmi.get_offset("linux_fdt") # libvmi does not support extra offset values.
        self.fdt_offset = OFFSET_LINUX_FILES_FDT
    
    # Return virtual address of fdt table 
    def get_fdt(self):
        return self.vmi.read_addr_va(self.current_files_struct + self.fdt_offset,0)

class Processs_navigator (object) :
    vmi = None
    list_entry_offset = 0
    name_offset = 0
    pid_offset = 0
    files_offset = 0
    current_proc = 0 # Virtual Address of current process 
    init_proc = 0 # Virtual Address of init_task
    
    def __init__(self,vmi,current_proc=None):
        global OFFSET_LINUX_PROCESS_FILES
        self.vmi = vmi
        self.list_entry_offset = vmi.get_offset("linux_tasks")
        self.name_offset = vmi.get_offset("linux_name")
        self.pid_offset = vmi.get_offset("linux_pid")
        #self.files_offset = vmi.get_offset("linux_files") # libvmi does not support extra offset values.
        self.files_offset = OFFSET_LINUX_PROCESS_FILES
        self.init_proc = vmi.translate_ksym2v("init_task")
        if current_proc == None :
            self.current_proc = self.init_proc
        else :
            self.current_proc = current_proc

    def get_name(self):
        return self.vmi.read_str_va(self.current_proc + self.name_offset,0)
    
    # Return virtual address of next process
    def get_next_proc(self): 
        return self.vmi.read_addr_va(self.get_list_entry(),0) - self.list_entry_offset
    
    def get_pid(self):
        return self.vmi.read_32_va(self.current_proc + self.pid_offset,0)
    
    # Return virtual address of list entry of current process
    def get_list_entry(self):
        return self.current_proc + self.list_entry_offset
    
    # Return virtual address of 'files' object of current process 
    def get_files_struct(self):
        return self.vmi.read_addr_va(self.current_proc + self.files_offset,0)
    
    def next(self):
        self.current_proc = self.get_next_proc()
    
def get_processes(vmi):
    pnavi = Processs_navigator(vmi)
    
    while True:
        procname = pnavi.get_name()
        pid = pnavi.get_pid()
        vaddr = pnavi.current_proc;
        if (pid < 1<<16):
            yield pid, procname, vaddr
        if (pnavi.init_proc == pnavi.get_next_proc()):
            break
        pnavi.next()

def find_process(vm_name,process_name):
    vmi = pyvmi.init(vm_name, "complete")
    ps = get_processes(vmi)
    for pid, procname, vaddr in ps:
        if procname == process_name :
            print "[%5d] %s %ul" % (pid, procname,vaddr)
            return vaddr
            
def list_processes(vm_name,verbose=False):
    vmi = pyvmi.init(vm_name, "complete")
    ps = get_processes(vmi)
    #print "opened files"
    for pid, procname, vaddr in ps:
        print "[%5d] %20s" % (pid, procname)
        if(verbose==True) :
            pnavi = Processs_navigator(vmi,vaddr)
            fsnavi = Files_struct_navigator(vmi,pnavi.get_files_struct())
            fdtnavi = Fdtable_navigator(vmi,fsnavi.get_fdt())
            print fdtnavi.get_file_count()
            
def get_info_sockets(vm_name):
    global I_MODE_MASK
    global I_MODE_SOCK
    global SOCK_TYPE_RAW
    global INETSOCK_FAMILY_AFINET
    vmi = pyvmi.init(vm_name, "complete")
    ps = get_processes(vmi)
    for pid, procname, vaddr in ps:
        #print "[%5d] %20s" % (pid, procname)
        pnavi = Processs_navigator(vmi,vaddr)
        fsnavi = Files_struct_navigator(vmi,pnavi.get_files_struct())
        fdtnavi = Fdtable_navigator(vmi,fsnavi.get_fdt())
        for file in fdtnavi.get_files() :
            #print "  - File at 0x%x" % (file)
            fnavi = File_navigator(vmi,file)
            dnavi = Dentry_navigator(vmi,fnavi.get_f_dentry())
            i_node_addr = dnavi.get_d_inode()
            if(i_node_addr==0) :
                #print "      Not have i_node."
                continue
            inavi = Inode_navigator(vmi,i_node_addr)
            i_mode = inavi.get_i_mode()
            if (i_mode & I_MODE_MASK) != I_MODE_SOCK :
                #print "    Not Socket."
                continue
            #print "    is Socket!"
            socket_addr = inavi.get_socket()
            if(socket_addr==0) :
                #print "      Not have struct socket"
                continue
            #print "      has struct socket!"
            snavi = Socket_navigator(vmi,socket_addr)
            s_type = snavi.get_type()
            if (s_type == SOCK_TYPE_RAW) :
                #print "    Raw Socket."
                continue
            #print "      Not raw socket!"
            isnavi = Inetsock_navigator(vmi,snavi.get_inetsock())
            inetsock_family = isnavi.get_family()
            #print "      family : %d" %(inetsock_family)
            if (inetsock_family != INETSOCK_FAMILY_AFINET) :
                #print "    Not IPv4 Socket."
                continue
            #print "    IPv4 Socket."
            daddr = isnavi.get_daddr()
            saddr = isnavi.get_saddr()
            dport = isnavi.get_dport()
            sport = isnavi.get_sport()
            yield pid, procname,saddr,sport,daddr,dport
                    
def list_info_sockets(vm_name):
    for pid,procname,saddr,sport,daddr,dport in get_info_sockets(vm_name):
        print 'pid : [%5d]' % pid
        print 'name : %s' % procname
        print 'src addr : %s' %(socket.inet_ntoa(struct.pack("<L",saddr)))
        print 'src port : %s' %(struct.unpack('>H',struct.pack('<H',sport))[0]) #changing little-endian port to normal integer
        print 'dst addr : %s' %(socket.inet_ntoa(struct.pack("<L",daddr)))
        print 'dst port : %s' %(struct.unpack('>H',struct.pack('<H',dport))[0]) #changing little-endian port to normal integer
        print ""

def get_socket_process_list(vm_name):
    list = []
    for pid,procname,saddr,sport,daddr,dport in get_info_sockets(vm_name):
        entry = Socket_process_info(vm_name, pid, procname, 
                                    socket.inet_ntoa(struct.pack("<L",saddr)), 
                                    int(struct.unpack('>H',struct.pack('<H',sport))[0]), 
                                    socket.inet_ntoa(struct.pack("<L",daddr)), 
                                    int(struct.unpack('>H',struct.pack('<H',dport))[0])
                                    )
        list.append(entry)
    #print "***********socket_process_list"
    #for entry in list:
    #    print entry
    return list

#def get_tool_stack(): #TODO
      
def get_vm_list() :
    proc = subprocess.Popen(['xl','list'],stdout=subprocess.PIPE)
    vm_list = []
    proc.stdout.readline() #skip the first line
    proc.stdout.readline() #skip the second line (Dom0)
    while True:
        line = proc.stdout.readline()
        if line == '' :
            break
        vm_list.append(line.split()[0])
    #print "***********vm_list"
    #print vm_list
    return vm_list
  
def get_datalink_addr_list(vm_name):
    proc = subprocess.Popen(['xl','network-list',vm_name],stdout=subprocess.PIPE)
    network_list = []
    proc.stdout.readline() #skip the first line
    while True:
        line = proc.stdout.readline()
        if line == '' :
            break
        network_list.append(line.split()[2])
    #print "***********network_list"
    #print network_list
    return network_list
  
def get_ip(datalink_addr): # TO-DO : to improve performance, we should avoid looking up the ARP tables repeatedly.
    #print "***********ip"
    proc = subprocess.Popen(['arp','-an'],stdout=subprocess.PIPE)
    while True:
        line = proc.stdout.readline()
        if line == '' :
            break
        line_data = line.split()
        ip = line_data[1][1:-1]
        mac = line_data[3]
        if (mac == datalink_addr) :
            #print ip
            return ip
    return None

def is_ip_mine(target_ip):
    proc = subprocess.Popen(['ifconfig'],stdout=subprocess.PIPE)
    while True:
        line = proc.stdout.readline()
        if line == '' :
            break
        line_data = line.split("inet addr:")
        if len(line_data) < 2 :
            continue
        ip = line_data[1].split(" ")[0]
        #print "ip : " + str(ip)
        #print "target ip : " + str(target_ip)
        if (ip == target_ip) :
            #print ip
            return True
    return False
  
def find_source_vm(src_ip):
    vm_list = get_vm_list()
    for vm_name in vm_list :
        da_list = get_datalink_addr_list(vm_name)
        for da in da_list:
            if get_ip(da) == src_ip :
                #print "***********source_vm"
                #print vm_name
                return vm_name
    return None
  
def find_source_processes(src_vm,src_ip,src_port,dst_ip,dst_port):
    sp_info_list = get_socket_process_list(src_vm)
    source_sp_info_list = []
    for sp_info in sp_info_list :
        if (sp_info.daddr == dst_ip and sp_info.dport == dst_port) :
            source_sp_info_list.append(sp_info)
        # more find grained search is possible with various combination of port and ip addresses
    #print "***********source_sp_info_list"
    #for sp_info in source_sp_info_list :
    #    print sp_info
    return source_sp_info_list

def find_source(src_ip,src_port,dst_ip,dst_port):
    src_procs = []
    src_vm = find_source_vm(src_ip)
    if src_vm == None :
        if is_ip_mine(src_ip) == False : return src_procs           
        for vm in get_vm_list():
            print "now checking vm : " + vm
            src_procs += find_source_processes(vm,src_ip,src_port,dst_ip,dst_port)
        return src_procs
    src_procs = find_source_processes(src_vm,src_ip,src_port,dst_ip,dst_port)
    #print "***********src_procs"
    #for sp_info in src_procs :
    #    print sp_info
    return src_procs
  
def main(argv):
    server = SimpleXMLRPCServer(("0.0.0.0", 8001))
    server.register_function(find_source, "find_source")
    server.serve_forever()

if __name__ == "__main__":
    if len(sys.argv) >= 2 and sys.argv[1] == "test" :
        for vm in get_vm_list() :
            list_info_sockets(vm)
        list = find_source("10.1.4.2",52677,"10.1.1.2",5001)
        print list
        print is_ip_mine("10.1.4.2")
    else :
        main(sys.argv)