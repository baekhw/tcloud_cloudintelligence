Node Monitor

- node_monitor.py

This module basically runs a XMLRPC server when it is executed as a main module.
When it is running the RPC server, the only function offered to the RPC client is 'find_source( ),'
which tries to find out the virtual machines and the processes that use matching source and destination addresses with the given source and destination IP address and port number parameters.
It returns a list of processes found.

For the introspection, it requires additional kernel information.
At this moment, the kernel information is hard-coded so it is working only for Ubuntu 12.04 Linux kernel 3.2.0-58-generic.

Under the 'tool' directory, there is a kernel module source to find out those kernel information.
You can change the hard-coded value to make the node monitor able to introspect your virtual machine whose OS is other than Ubuntu 12.04 Linux kernel 3.2.0-58-generic.