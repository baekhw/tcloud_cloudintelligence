#!/bin/sh
#installation script for cloud intelligence node

#install sflowtool
cd /users/Weiyu
#sudo wget http://www.inmon.com/bin/sflowtool-3.22.tar.gz
#sudo tar -xvzf sflowtool-3.22.tar.gz
cd sflowtool-3.30
sudo ./configure
sudo make
sudo make install

#install MySQL
sudo apt-get install mysql-server
#set passwd for root as root
#start mysql
sudo service mysql start

#install mysqldb for python
sudo apt-get install python-mysqldb
