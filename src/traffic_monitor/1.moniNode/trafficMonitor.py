import os
import time
import logging
import xmlrpclib
from SimpleXMLRPCServer import SimpleXMLRPCServer
import MySQLdb

#file to keep the temp sflow data
tempFilePath = "/users/Weiyu/1.moniNode/temp/sFlow_temp_file.txt"
#interval for monitoring the bandwidth, it was set to equal to the value of polling in Host sFlow
interval = 5
#log file
logFile = "/users/Weiyu/1.moniNode/log/monitorLog.txt"
#config the format of the log
logging.basicConfig(filename = logFile ,level = logging.DEBUG, filemode = 'a', format = '%(asctime)s - %(levelname)s: %(message)s')

server = SimpleXMLRPCServer(("10.1.2.2", 8000))

conn=MySQLdb.connect(host='localhost',user='root',passwd='root',db='demo',port=3306)
cur = conn.cursor()

proxy = xmlrpclib.ServerProxy("http://10.1.3.2:8001/")

#insert info about the adrr into the database
def insertIntoDatabase(addrandCountTable):
	currentTime = time.time()

	for srcAddr in addrandCountTable:
                logging.debug("srcAddr %s" %srcAddr)
                for dstAddr in addrandCountTable[srcAddr]:
                        count = addrandCountTable[srcAddr][dstAddr]
                        logging.debug("dstAddr %s, count %s" %(dstAddr, count))

                        try:
                                logging.debug("before inserting dst record into database")
                                cur.execute('''insert into addrTable (time, srcAddr, dstAddr, count) values (%s, %s, %s, %s) ''', (currentTime, srcAddr, dstAddr, count))
                                conn.commit()
                                logging.debug("After inserting dst record into database")
                        except MySQLdb.Error, e:
                                conn.rollback()
                                logging.error("Mysql Error")	

def storeData():
        addrandCountTable = {}#srcIP:inputPort,dstIP:outputPort,count

        tempFile = open(tempFilePath, "r")
        lists = tempFile.readlines()
        rowCount = len(lists);
        logging.debug("rowCount %s" %rowCount)
        datagramCount = 0

        if(rowCount > 0):

                while(rowCount >= 0):
                        #read the datagrams received in the last interval
                        if(datagramCount <= interval):
                                s = lists[rowCount - 1]
#                               logging.debug("%i datagram, s: %s" %(datagramCount, s))
                                row = s.split(" ")
                                rowName = row[0]
                                logging.debug("%i datagram, Rowname: %s" %(datagramCount, rowName))

                                if("endDatagram" in rowName):
                                        datagramCount = datagramCount + 1

                                dstIP = " "
                                outputPort = " "
                                dstAddr = " "

                                srcIP = " "
                                inputPort = " "
                                srcAddr = " "

                                if("srcIP" in rowName):
                                        srcIP = row[1]
                                        srcIP = srcIP[0: (len(srcIP)-1)]
                                        logging.debug("get srcIP %s" %srcIP)

                                        t = lists[rowCount - 1 + 5]
                                        row = t.split(" ")
                                        rowName = row[0]

                                        #get the related inputPort              
                                        if("TCPSrcPort" in rowName):
                                                inputPort = row[1]
                                                inputPort = inputPort[0 : len(inputPort) - 1]
                                                logging.debug("get inputPort %s" %inputPort)
                                                logging.debug("srcIP is %s"  %srcIP)
						srcAddr = srcIP + ":" + inputPort
                                                logging.debug("formed srcAddr %s" %srcAddr)
                                        else:
                                                logging.debug("no inputPort found for srcIP: %s in the same sample" %srcIP)
                                                continue

                                        #get the related dstIP
                                        t = lists[rowCount]
                                        row = t.split(" ")
                                        rowName = row[0]
                                        if("dstIP" in rowName):
                                                dstIP = row[1]
                                                dstIP = dstIP[0 : len(dstIP) - 1]
                                                logging.debug("get dstIP %s" %dstIP)

                                                #get the related outputPort
                                                t = lists[rowCount + 5]
                                                row = t.split(" ")
                                                rowName = row[0]
                                                if("TCPDstPort" in rowName):
                                                        outputPort = row[1]
                                                        outputPort = outputPort[0: len(outputPort)-1]
                                                        #form dstAddr(dstIP:outputPort)
                                                        dstAddr = dstIP + ":" + outputPort
                                                else:
                                                        logging.debug("no outputPort found for dstIP: %s for srcIP: %s in the same sample" %(dstIP, srcIP))
                                                        continue
                                        else:
                                                logging.debug("no dstIP found for srcIP: %s in the same sample" %srcIP)
                                                continue

                                        if(srcAddr not in addrandCountTable):
                                                addrandCountTable[srcAddr] = {}
                                                addrandCountTable[srcAddr][dstAddr] = 1
                                        elif(dstAddr not in addrandCountTable[srcAddr]):
                                                addrandCountTable[dstAddr][dstAddr] = 1
					else:
                                                addrandCountTable[srcAddr][dstAddr] = addrandCountTable[srcAddr][dstAddr] + 1

                        else:
                                break

                        rowCount = rowCount - 1
        else:
             logging.debug("No sflow data in this interval")

	insertIntoDatabase(addrandCountTable)

def trafficMonitor(msg):
	if(msg == "normal"):
		b = proxy.stateTransition("normal")
		print "Got Normal Msg from GW and Forwarded it to CI"
		return 1
	elif(msg == "alarm1" or msg == "alarm2"):
#		print ("got %s msg from GW" %msg)
		storeData()
#		print "stored data and before send msg to CI"
		b = proxy.stateTransition(msg)
		print("Got %s Msg from GW and Forwarded it to CI" %msg)
		return 1
	elif("#" in msg):
		m = msg.split("#")
		ciState = m[0]
		ipAddr = m[1]
		port = m[2]
		addr = ipAddr + ":" + port
		#     		data = selectDataFromDatabase()
	else:
		return 0

if __name__ == "__main__":
        server.register_function(trafficMonitor, "trafficMonitor")
        server.serve_forever()
        f = open(logFile, "w")
        f.truncate()
        f.close()

        cur.close()
        conn.close()
