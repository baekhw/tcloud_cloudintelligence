import os
import time
import logging
import xmlrpclib

#config the format of the log
logging.basicConfig(filename = '/users/Weiyu/0.gwNode/log/alramGeneratorLog.txt',level = logging.DEBUG, filemode = 'a', format = '%(asctime)s - %(levelname)s: %(message)s')

#file to keep the temporary bandwidth information
tempFilePath = "/users/Weiyu/0.gwNode/temp/ifstat_temp_file.txt"

#interval for using ifstat to monitor the bandwidth usage in seconds
interval = 5
#the interface to be monitored
interface = "br0"
#values for predefined thresholds
threshold1 = 100.0
threshold2 = 200.0

#server in moniNode
proxy = xmlrpclib.ServerProxy("http://10.1.2.2:8000/")

#if the reference contain number in it
def containNumber(n):
        if("0" in n or "1" in n or "2" in n or "3" in n or "4" in n or "5" in n or "6" in n or "7" in n or "8" in n or "9" in n):
                return True
        else:
                return False

#read tempFile get with ifstat and caculate the average bandwidth usage in the last interval
def getAverageBandwidthUsage():
	f = open(tempFilePath, "r")
        line = f.readlines()
        n = len(line)

        sum_bandwidth_usage = 0.0
        average_bandwidth_usage = 0.0
        count = 0

        if(n != 0 and "br0" not in line[0] and "br0" not in line[1]):
                 for i in range(0, n):
                         elements = line[i].split(" ")

                         for j in range(0, len(elements)):
                                 b = containNumber(elements[j])
                                 if(b):
                                         s = elements[j]
                                         temp_bandwidth_usage_out = float(s)
 
                         sum_bandwidth_usage = sum_bandwidth_usage + temp_bandwidth_usage_out
                         count = count + 1
        else:
                 logging.debug("%s is empty." % tempFilePath)

        f.close()
        f = open(tempFilePath, "w")
        f.truncate()
        f.close()

        if( count != 0 ):
                average_bandwidth_usage = sum_bandwidth_usage/count

	return average_bandwidth_usage

def monitorBandwidth():
	#print the prompt in the very beginning
	print "bandwidth_usage(kBps)"

	#get the average bandwidth usage in very interval and send alarms to moniNode if necessary
	prevAlarmMsg = 0
        while(True):
                time.sleep(interval)
		
		average_bandwidth_usage = getAverageBandwidthUsage()
		print average_bandwidth_usage		

                if(average_bandwidth_usage > threshold2 and prevAlarmMsg != 2):
#			print "beyond threshold2"
			prevAlarmMsg = 2
#			print "before send msg to moninode"
                        proxy.trafficMonitor("alarm2")
#			print "after send msg to moniNode"
                elif(average_bandwidth_usage > threshold1 
			and average_bandwidth_usage <= threshold2 
			and prevAlarmMsg != 1):
#			print "beyond threshod1"
			prevAlarmMsg = 1
#			print "before send msg to moniNode"
                        proxy.trafficMonitor("alarm1")
#			print "after send msg to moniNode"
		elif(average_bandwidth_usage <= threshold1 and prevAlarmMsg > 0):
#			print "back to normal"
			prevAlarmMsg = 0
#			print "before send msg to moniNode"
			proxy.trafficMonitor("normal")
#			print "after send msg to moniNode"

if __name__ == "__main__":
        monitorBandwidth()
