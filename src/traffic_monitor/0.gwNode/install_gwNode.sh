#!/bin/sh
#installation script for gateway

#install OVS
sudo apt-get update
sudo apt-get install -y git automake autoconf gcc uml-utilities libtool build-essential
sudo cd /users/Weiyu/openvswitch-2.0.0
sudo ./boot.sh
sudo ./configure --with-linux=/lib/modules/`uname -r`/build
sudo make
sudo make install
#install and load kernel modules
sudo make modules_install
sudo /sbin/modprobe openvswitch
# Initialize the configuration database using ovsdb-tool
sudo mkdir -p /usr/local/etc/openvswitch
sudo ovsdb-tool create /usr/local/etc/openvswitch/conf.db vswitchd/vswitch.ovsschema
#start ovs’ configuration database
sudo ovsdb-server --remote=punix:/usr/local/var/run/openvswitch/db.sock --remote=db:Open_vSwitch,Open_vSwitch,manager_options --pidfile –detach
#initialize database using ovs-vsctl
sudo ovs-vsctl --no-wait init
#start the main Open vSwitch daemon and connect it to the same Unix domain socket
sudo ovs-vswitchd --pidfile –detach

#create bridge and connect it to eth2, the physical interface to the external node
sudo ifconfig eth1 down
sudo ifconfig eth1 0
sudo ovs-vsctl add-br br0
sudo ovs-vsctl add-port br0 eth1
#10.1.1.3 is the orginal IP of eth2
sudo ifconfig br0 10.1.1.3 netmask 255.255.255.0 up

#create sflow and connect to br0
#collector IP:10.1.2.2
sudo ovs-vsctl --id=@sflow create sflow agent=eth1 target=\"10.1.2.2:6343\" header=128 sampling=1 polling=1 -- set bridge br0 sflow=@sflow

#install Host sFlow
cd /users/Weiyu
sudo dpkg --install hsflowd_1.22.2-1_x86_64.deb
sudo cp hsflowd.conf /etc/hsflowd.conf
sudo service hsflowd restart

#install ifstat
cd /users/Weiyu/ifstat-1.1
sudo ./configure
sudo make
sudo make install
