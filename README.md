# Cloud Intelligence
As a part of TCloud project, this project is implementing a use-case of Cloud Intelligence as well as monitoring tools that supports the intelligence.
The intelligence logic is implemented in a simple state machine just to show the use-case; later, we may use production-rule based intelligence engine to enable the intelligence to dynamically reflect new rules and to take measure for various situations.

The node monitor introspect virtual machines, traverse processes and extracts their socket address information.
The original purpose of this node monitor is finding out process list those generate some malicious packets by making use of the address information of those packets.

The traffic monitor checks the network traffic overhead. When the traffic exceeds certain threshold, the traffic monitor alerts the cloud intelligence.
In addition, it reports the analytical information of the traffic to the cloud intelligence.
Then the cloud intelligence can try to find out the virtual machines and the processes that are generating the traffic.

More detailed information can be found from the document about the project in the '/paper'.